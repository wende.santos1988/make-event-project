package com.start.makeevent.listiner;

import com.newrelic.api.agent.NewRelic;
import com.start.makeevent.events.CreateEventEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CreatedEventListener implements ApplicationListener<CreateEventEvent> {

    @Override
    public void onApplicationEvent(CreateEventEvent event) {
        log.info("class=CreatedEventListener, method=onApplicationEvent, event={}, id={}"
                , event.getEvent().getName(), event.getEvent().getId());

        NewRelic.recordMetric("Metric/Create/EventCreated", event.getEvent().getId());
    }
}
