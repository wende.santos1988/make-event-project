package com.start.makeevent.events;

import com.start.makeevent.domain.model.Event;
import com.start.makeevent.post.MakeEventPublisher;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class CreateEventEvent extends ApplicationEvent {

    private Event event;

    public CreateEventEvent(MakeEventPublisher makeEventPublisher, Event event) {
        super(makeEventPublisher);
        setEvent(event);
    }
}
