package com.start.makeevent.controllers;

import com.start.makeevent.domain.model.Event;
import com.start.makeevent.post.MakeEventPublisher;
import com.start.makeevent.service.CreateEventService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/make-event")
public class CreateEventController {

    private final CreateEventService createEventService;

    @GetMapping("/create")
    public void createEvent() {
        log.info("class=CreateEventController, method=createEvent, message=starting process to create new event");
        createEventService.createEvent();
    }

}
