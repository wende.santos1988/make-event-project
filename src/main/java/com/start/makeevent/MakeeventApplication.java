package com.start.makeevent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MakeeventApplication {

	public static void main(String[] args) {
		SpringApplication.run(MakeeventApplication.class, args);
	}

}
