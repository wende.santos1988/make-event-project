package com.start.makeevent.post;

import com.newrelic.api.agent.NewRelic;
import com.start.makeevent.domain.model.Event;
import com.start.makeevent.events.CreateEventEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class MakeEventPublisher {

    private final ApplicationEventPublisher applicationEventPublisher;

    public void publishCreateEvent(Event event) {
        log.info("class=MakeEventPublisher, method=publishCreateEvent, message=starting publish createEvent");
        CreateEventEvent createEventEvent = new CreateEventEvent(this, event);
        applicationEventPublisher.publishEvent(createEventEvent);
        NewRelic.recordMetric("Metric/Create/Event", event.getId());
    }

}
