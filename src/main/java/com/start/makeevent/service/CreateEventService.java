package com.start.makeevent.service;

import com.start.makeevent.domain.model.Event;
import com.start.makeevent.post.MakeEventPublisher;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Slf4j
@Service
@AllArgsConstructor
public class CreateEventService {

    private final MakeEventPublisher makeEventPublisher;

    public void createEvent() {
        log.info("class=CreateEventService, method=createEvent, message=Start process");

        Event event = Event.builder()
                .id(1).name("Event " + 1).description("name event").date(LocalDateTime.now()).build();
        makeEventPublisher.publishCreateEvent(event);
    }

}
